﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;


namespace Empleados.Model
{
    public class Empleado { }
    public class empleado : INotifyPropertyChanged
    {
        private string nombre;
        private string apellido;

        public string Nombre {
            get {
                return nombre;
            }

            set {
                if (nombre != value) {
                    nombre = value;
                    RaisePropertyChanged("Nombre");
                    RaisePropertyChanged("NombreCompleto");
                }
            }

        }
        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                if (apellido != value)
                {
                    apellido = value;
                    RaisePropertyChanged("Apellido");
                    RaisePropertyChanged("NombreCompleto");
                }
            }

        }

        public string NombreCompleto
        {

            get
            {
                return nombre + " " + apellido;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}