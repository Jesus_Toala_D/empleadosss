﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Empleados.Model;


namespace Empleados.ViewModel
{
    class EmpleadoVM
    {
        public ObservableCollection<empleado> empleadoss
        {
            get;
            set;
        }

        public void CargaEmpleados()
        {
            ObservableCollection<empleado> empleados = new ObservableCollection<empleado>();
            empleados.Add(new empleado { Nombre = "Alejo", Apellido = "Soledispa" });
            empleados.Add(new empleado { Nombre = "Lider", Apellido = "Delgado" });
            empleados.Add(new empleado { Nombre = "Cesar", Apellido = "Velez" });
            empleados.Add(new empleado { Nombre = "Maria", Apellido = "Vargas" });
            empleadoss = empleados;
        }
    }
}
